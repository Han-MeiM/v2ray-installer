#!/bin/bash

red='\e[91m'
green='\e[92m'
yellow='\e[93m'
magenta='\e[95m'
cyan='\e[96m'
none='\e[0m'
_red() { echo -e ${red}$*${none}; }
_green() { echo -e ${green}$*${none}; }
_yellow() { echo -e ${yellow}$*${none}; }
_magenta() { echo -e ${magenta}$*${none}; }
_cyan() { echo -e ${cyan}$*${none}; }

md5() {
    echo -n $1 | md5sum | cut -d ' ' -f 1
}

generateDomain() {
    time=$(date "+%Y%m%d")
    sign=$(md5 surfshellhost$time)
    sign=$(md5 ${sign})
    echo $sign.xyz
}

_open_bbr() {
    sed -i '/net.ipv4.tcp_congestion_control/d' /etc/sysctl.conf
    sed -i '/net.core.default_qdisc/d' /etc/sysctl.conf
    echo "net.ipv4.tcp_congestion_control = bbr" >>/etc/sysctl.conf
    echo "net.core.default_qdisc = fq" >>/etc/sysctl.conf
    sysctl -p >/dev/null 2>&1
    echo
    _green "..由于你的 VPS 内核支持开启 BBR ...已经为你启用 BBR 优化...."
    echo
}

_try_enable_bbr() {
    local _test1=$(uname -r | cut -d\. -f1)
    local _test2=$(uname -r | cut -d\. -f2)
    if [[ $_test1 -eq 4 && $_test2 -ge 9 ]] || [[ $_test1 -ge 5 ]]; then
        _open_bbr
        enable_bbr=true
    fi
}

get_ip() {
    ip=$(curl -s https://ipinfo.io/ip)
    [[ -z $ip ]] && ip=$(curl -s https://api.ip.sb/ip)
    [[ -z $ip ]] && ip=$(curl -s https://api.ipify.org)
    [[ -z $ip ]] && ip=$(curl -s https://ip.seeip.org)
    [[ -z $ip ]] && ip=$(curl -s https://ifconfig.co/ip)
    [[ -z $ip ]] && ip=$(curl -s https://api.myip.com | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
    [[ -z $ip ]] && ip=$(curl -s icanhazip.com)
    [[ -z $ip ]] && ip=$(curl -s myip.ipip.net | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
    [[ -z $ip ]] && echo -e "\n$red 这垃圾小鸡扔了吧！$none\n" && exit
}

# ## docker install
# ps -aux | grep docker
# if [ $? -eq 0 ]; then
#     echo docker 已安装
# else
#     sudo apt-get remove docker docker-engine docker.io containerd runc
#     echo Y | sudo apt-get install \
#         apt-transport-https \
#         ca-certificates \
#         curl \
#         gnupg-agent \
#         software-properties-common
#     sleep 1
#     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#     ## docker-compose install
#     sudo add-apt-repository \
#         "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
#    $(lsb_release -cs) \
#    stable"
#     sudo apt-get update
#     echo Y | sudo apt-get install docker-ce docker-ce-cli containerd.io

#     sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#     sudo chmod +x /usr/local/bin/docker-compose
# fi

## tls-shunt-proxy install
ps -aux | grep tls-shunt-proxy
if [ $? -eq 0 ]; then
    echo tls-shunt-proxy 已安装
else
    sudo apt-get install unzip
    sudo curl -o tls-shunt-proxy.sh https://raw.githubusercontent.com/liberal-boy/tls-shunt-proxy/master/dist/install.sh
    sudo bash tls-shunt-proxy.sh
fi

# ## get ip
# read -p "Enter ip, please: " ip

# # bbr
# _try_enable_bbr

# UUID=$(uuidgen)
domain=$(generateDomain)
ip=$(get_ip)
prefix=$(echo $ip | tr -d ".")
serverName=$prefix.$domain

# # 替换 v2ray config.json 中的 uuid
# sudo sed -i 's/${uuid}/'$UUID'/' ./config.json
# sudo cp ./config.json ./services/v2ray/config.json

# # 替换 tls-shunt-proxy config.yaml 中的 domain
sudo sed -i 's/${domain}/'$serverName'/' ./tls-shunt-proxy.yaml
sudo cp ./tls-shunt-proxy.yaml /etc/tls-shunt-proxy/config.yaml

## v2ray install
sudo bash v2ray.sh

# # sudo sed -i 's/$SERVER_NAME/'$serverName'/' ./v2ray.conf
# # sudo cp ./v2ray.conf ./services/nginx/conf.d

# ## run docker
# sudo docker-compose up -d

# ## restart tls-shunt-proxy
# sudo systemctl restart tls-shunt-proxy

echo
# _green "uuid: "$UUID
# _green "ip: "$ip
# _green "domain: "$domain
_green "url: "$serverName
echo
